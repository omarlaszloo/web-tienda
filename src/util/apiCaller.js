
export function callApi (endpoint, method = 'get', body) {
  let getcookie = getCookie('token')
  return fetch(`http://localhost:3000/v1/products/all-products}`, { method,
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify(body)
  })
  .then(response => response.json().then(json => ({ json, response })))
  .then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json)
    }
    return json
  })
  .then(
    response => response,
    error => error
  )
}
