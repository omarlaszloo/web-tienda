import React, { Component } from 'react'
import Products from './products.component'
import '../../styles/global.css'
import InfiniteScroll from 'react-infinite-scroller'
import qwest from 'qwest'


const api = {
  baseUrl: 'http://localhost:3000/v1/products/all-products/'
}
class ContainerProducts extends Component {

  state = {
    page: '',
    total: '',
    products: [],
    hasMoreItems: true,
    nextHref: null,
    loading: 'Loading ...'
  }

  loadItems(page) {
    console.log(page);
    let self = this;

    let url = api.baseUrl + page;

    qwest.get(url, {
      page: 1,
      page_size: 10
    }, {
      cache: true
    })
    .then(function(xhr, resp) {
      console.log(resp);
      if(resp) {

        let products = self.state.products;
        resp.products.map((item) => {
          products.push(item);
        });


        if(resp.current) {
          self.setState({
            products: products,
            nextHref: resp.current
          });
        } else {
          self.setState({
            hasMoreItems: false
          });
        }
      }
    });
  }

  render() {
    const loader = <div className="loader" key={0}>{this.state.loading}</div>
    let products = this.state.products
    const items = [];
    products.map((item, i) => {
      items.push(<Products {...item} key={item.id} />);
    });

    return (
      <div className='grid-products'>
        <InfiniteScroll
          pageStart={0}
          loadMore={this.loadItems.bind(this)}
          hasMore={this.state.hasMoreItems}
          loader={loader}>
          <div>
            {items}
          </div>
        </InfiniteScroll>
      </div>
    )
  }
}

export default ContainerProducts
