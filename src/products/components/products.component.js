import React, { PureComponent, PropTypes } from 'react'
import './products.css'

class Products extends PureComponent {

  handleClick = (event)  =>  {
    console.log(this.props.id)
  }
  render () {

    return (
      <div className='box container' onClick={this.handleClick}>
        <div>
          <img
            src='./images/covers/hamburguesa.png'
            alt=''
            width={260}
            height={160}
          />
          <h3>{this.props.title}</h3>
          <div className='content-description'>
            <p className='description'>ver mas</p>
            <p className='price'>${this.props.price}</p>
          </div>
        </div>
      </div>
    )
  }
}

export default Products
