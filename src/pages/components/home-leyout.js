import React from 'react'
import '../../styles/global.css'

function HomeLayout(props) {
  return(
    <section className='grid-main'>
      {props.children}
    </section>
  )
}

export default HomeLayout
