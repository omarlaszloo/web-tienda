import React, { Component } from 'react'
import HomeLayout from '../components/home-leyout'
import ContainerProducts from '../../products/components/products.container'
import data from '../../data.json'

class Home extends Component {

  render () {
    return (
      <HomeLayout>
        <ContainerProducts data={data}/>
      </HomeLayout>
    )
  }
}

export default Home
