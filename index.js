import React from 'react'
import { render } from 'react-dom'
import ContainerProducts from './src/products/components/products.container'
import data from './src/data.json'

const app = document.getElementById('app')

render(<ContainerProducts data={data} />, app)
